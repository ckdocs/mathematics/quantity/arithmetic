# Logarithm

## Floor and Ceil

1. **Floor(x)**: the greatest integer less than or equal to x
2. **Ceil(x)**: the least integer greater than or equal to x

$$
\begin{aligned}
    & Floor(2.5) = \lfloor 2.5 \rfloor = 2
    \\
    & Floor(-3.04) = \lfloor -3.04 \rfloor = -4
    \\
    & Floor(4) = \lfloor 4 \rfloor = 4
    \\
    \\
    & Ceil(2.5) = \lceil 2.5 \rceil = 3
    \\
    & Ceil(3.04) = \lceil -3.04 \rceil = -3
    \\
    & Ceil(4) = \lceil 4 \rceil = 4
\end{aligned}
$$

---

## Logarithm rules

There are many important rules for logarithms:

$$
\begin{aligned}
    & \log_{c}^{a} + \log_{c}^{b} = \log_{c}^{ab}
    \\
    & \log_{c}^{a} - \log_{c}^{b} = \log_{c}^{\frac{a}{b}}
    \\
    & \log_{b}^{a} = \frac{1}{\log_{a}^{b}}
    \\
    & \log_{b}^{a^n} = n.\log_{b}^{a}
    \\
    & \log_{b^m}^{a^n} = \frac{n}{m}.\log_{b}^{a}
    \\
    & \log_{b}^{a} = \frac{\log_{c}^{a}}{\log_{c}^{b}}
    \\
    & \log_{b}^{a} . \log_{c}^{b} = \log_{c}^{a}
    \\
    & a^{\log_{c}^{b}} = b^{\log_{c}^{a}}
    \\
    \\
    Proof:
    \\
    & \log_{c}^{a} + \log_{c}^{b} = \log_{c}^{ab}
    \\
    & x = \log_{c}^{a}
    \\
    & y = \log_{c}^{b}
    \\
    & z = \log_{c}^{ab}
    \\
    \\
    & x + y = z
    \\
    \\
    & a = c^x
    \\
    & b = c^y
    \\
    & ab = c^z
    \\
    \\
    & ab = c^{x+y} = c^{z}
    \\ \implies
    & x + y = z
\end{aligned}
$$

---

> In mathematics, if we don't write the log **base** it means, the default is **10**
>
> $$
> \begin{aligned}
>     & \log x = log_{10}^{x}
> \end{aligned}
> $$

---

> In data structures, if we don't write the log **base** it means, the default is **2**
>
> $$
> \begin{aligned}
>     & \log x = \lg x = log_{2}^{x}
> \end{aligned}
> $$

---

$$
\begin{aligned}
    & \ln x = log_{e}^{x}
\end{aligned}
$$

---

## Iterated Logarithm (Log Star)

The iterated logarithm of **n**, written **log\*n** (usually read **log star**), is the number of times the logarithm function must be iteratively applied before the result is less than or equal to **1**

$$
\begin{aligned}
    & \log^{*}n =
    \begin{cases}
        0 & n \leq 1
        \\
        1 + \log^{*}(\log n) & n \gt 1
    \end{cases}
\end{aligned}
$$

### Example

$$
\begin{aligned}
    & \log^{*} 16 \implies
    \\
    & \log 16 = 4
    \\
    & \log 4 = 2
    \\
    & \log 2 = 1
    \\
    & \implies \log^{*} 16 = 3
\end{aligned}
$$

---
