# Growth of Function (GOF)

We use power of **Logarithms** to reduce the power of functions to be comparable

Analysing function `F(n)` in the **Infinite n values**

> **Growth vs Value**
> We cannot compare **Growth** of functions by their **Values**, we can compare with **Speed** of values

![Complexity](assets/time-complexity.webp)

---

> **Example**
>
> Comparse GOF of these functions:
>
> 1. $n$
> 2. $n^2$
> 3. $n^3$
> 4. $2^n$
> 5. $\log{n}$
> 6. $n.\log{n}$
>
> ![Compare Values](assets/growth_of_function_compare_values.png)
>
> $$
> \begin{aligned}
> & \log{n} \ll n \ll n.\log{n} \ll n^2 \ll n^3 \ll 2^n
> \end{aligned}
> $$

---

> **Function Categories**
>
> We can categorize functions by their growth and their style into multiple groups:
>
> 1. **Logarithmic**: $\log{n}$
> 2. **Linear**: $n$
> 3. **Polynomial**: $n^2$, $n^3$
> 4. **Exponential**: $2^n$

---

## Compare GOF

We can use multiple tools to compare growth of functions:

1. Limit
2. Derivation
3. Handy Compare

### Limit

$$
\begin{aligned}
    & \lim_{n \to \infty} \frac{f(n)}{g(n)} =
    \begin{cases}
        \infty & f(n) \gg g(n)
        \\
        0 \lt k \lt \infty & f(n) \sim g(n)
        \\
        0 & f(n) \ll g(n)
    \end{cases}
\end{aligned}
$$

> **Example**
>
> We can compare **GOF** of function using limit:
>
> ---
>
> -   Compare growth of $n^2$ and $2^n$:
>
> $$
> \begin{aligned}
> 	& \lim_{n \to \infty} \frac{2^n}{n^2}
> 	\\ \implies
> 	& \lim_{n \to \infty} \frac{2^n . \ln{2}}{2.n}
> 	\\ \implies
> 	& \lim_{n \to \infty} \frac{2^n . \ln{2}^2}{2} = \infty
> 	\\
> 	\\ \implies
> 	& 2^n \gg n^2
> \end{aligned}
> $$
>
> ---
>
> -   Compare growth of $\log_{4}^{n}$ and $\log_{3}^{n}$:
>
> $$
> \begin{aligned}
> 	& \lim_{n \to \infty} \frac{\log_{3}^{n}}{\log_{4}^{n}}
> 	\\ \implies
> 	& \lim_{n \to \infty} \frac{\frac{1}{n.\ln{3}}}{\frac{1}{n.\ln{4}}} = \frac{\ln{4}}{\ln{3}} = \log_{3}^{4}
> 	\\
> 	\\ \implies
> 	& \log_{4}^{n} \sim \log_{3}^{n}
> \end{aligned}
> $$

### GOF Tips

There are many tips in comparing GOF of functions

#### Constant

$$
\begin{aligned}
    & f(x) \sim c.f(x)
    \\
    \\
    & Proof:
    \\
    & \lim_{n \to \infty} \frac{c.f(x)}{f(x)} = c \neq 0
\end{aligned}
$$

#### Logarithmic

$$
\begin{aligned}
    & \log_{a}^{n} \sim \log_{b}^{n}
    \\
    \\
    & Proof:
    \\
    & \lim_{n \to \infty} \frac{\log_{a}^{n}}{\log_{b}^{n}} = \frac{\frac{1}{\log_{n}^{a}}}{\frac{1}{\log_{n}^{b}}} = \frac{\log_{n}^{b}}{\log_{n}^{a}} = \log_{a}^{b} \neq 0
\end{aligned}
$$

#### Exponential vs Polynomial

$$
\begin{aligned}
    & a^n \gg n^b
    \\
    \\
    & Example:
    \\
    & 1.5^n \gg n^{200}
\end{aligned}
$$

#### Logarithm of Function

$$
\begin{aligned}
    & f(n)^a \gg (\log{f(n)})^{b}
    \\
    \\
    & Example:
    \\
    & n^2 \gg (\log{n})^{1000}
\end{aligned}
$$

#### Compare by Logarithm (Golden)

$$
\begin{aligned}
    & f(n), g(n) \gt 1
    \\
    & f(n), g(n): Increasing
    \\
    \\
    & \log{f(n)} \ll \log{g(n)} \implies f(n) \ll g(n)
    \\
    & \log{f(n)} \gg \log{g(n)} \implies f(n) \gg g(n)
    \\
    & \log{f(n)} \sim \log{g(n)} \nRightarrow f(n) \sim g(n)
\end{aligned}
$$

#### Power Changing

By increasing the power of function **GOF** will increase, and by decreasing the power of function **GOF** will decrease

$$
\begin{aligned}
    & \sqrt{n} \ll n \ll n\sqrt{n} \ll n^2 \ll n^{\frac{5}{2}}
    \\
    & \sqrt{\log{n}} \ll \log{n} \ll (\log{n})^{\frac{3}{2}} \ll (\log{n})^2 \ll (\log{n})^{\frac{5}{2}}
\end{aligned}
$$

#### Factorial

> **Sterling Formula**
>
> We can use **Sterling** proof to simplify the factorial formula
>
> $$
> \begin{aligned}
>     & Sterling:
>     \\
>     & n! \sim \sqrt{2 \pi n} \left(\frac{n}{e}\right)^{n} \sim \sqrt{n} \left(\frac{n}{e}\right)^{n}
> \end{aligned}
> $$

Now we can compare **GOF** of factorials

$$
\begin{aligned}
    & n^n \gg n!
    \\
    \\
    & Proof:
    \\
    & \lim_{n \to \infty} \frac{n^n}{n!}
    \\ =
    & \lim_{n \to \infty} \frac{n^n}{\sqrt{n} \left(\frac{n}{e}\right)^n}
    \\ =
    & \lim_{n \to \infty} \frac{n^n}{\sqrt{n} \frac{n^n}{e^n}}
    \\ =
    & \lim_{n \to \infty} \frac{e^n}{\sqrt{n}} = \infty
    \\
    \\ \implies
    & n^n \gt n!
\end{aligned}
$$

$$
\begin{aligned}
    & \log{n!} \sim n \log{n}
    \\
    \\
    & Proof:
    \\
    & \lim_{n \to \infty} \frac{\log{n!}}{n \log{n}}
    \\ =
    & \lim_{n \to \infty} \frac{\log{\sqrt{n} \left(\frac{n}{e}\right)^n}}{n \log{n}}
    \\ =
    & \lim_{n \to \infty} \frac{\log{\sqrt{n}} + \log{\left(\frac{n}{e}\right)^n}}{n \log{n}}
    \\ =
    & \lim_{n \to \infty} \frac{\log{\sqrt{n}} + n \log{\frac{n}{e}}}{n \log{n}}
    \\ =
    & \lim_{n \to \infty} \frac{\log{\sqrt{n}} + n \log{n} - n \log{e}}{n \log{n}}
    \\ =
    & \lim_{n \to \infty} \frac{\frac{1}{2}\log{n} + n \log{n} - n \log{e}}{n \log{n}}
    \\ =
    & \lim_{n \to \infty} \frac{n \log{n}}{n \log{n}} = 1
    \\
    \\ \implies
    & \log{n!} \sim n \log{n}
\end{aligned}
$$

---

#### Example

-   Compare growth of $\frac{n}{\log{n}}$ and $\sqrt{n} \log{n}$

$$
\begin{aligned}
    & \lim_{n \to \infty} \frac{\frac{n}{\log{n}}}{\sqrt{n} \log{n}} = \frac{n}{\sqrt{n} (\log{n})^2} = \frac{\sqrt{n}}{(\log{n})^2} = \infty
    \\
    \\
    & \frac{n}{\log{n}} \gg \sqrt{n} \log{n}
\end{aligned}
$$

---

-   Compare growth of $\log\log{n}$ and $(\log\log{n})^2$ and $\log{(\log{n})^2}$ and $\log\log{(n^2)}$ and $\sqrt{\log{n}}$

$$
\begin{aligned}
    & \log{(\log{n})^2} = 2\log\log{n}
    \\
    & \log\log{(n^2)} = \log{2\log{n}} = \log{2} + \log\log{n}
    \\
    \\
    & \log\log{n} \sim \log{(\log{n})^2} \sim \log\log{(n^2)} \ll (\log\log{n})^2 \ll \sqrt{\log{n}}
\end{aligned}
$$

---

-   Compare growth of $n^{(\log{n})}$ and $(\log{n})^n$

> **Compare with logarithm**
>
> If we cannot compare growth of functions, we can compare growth of **Logarithm of Functions**

$$
\begin{aligned}
    & n^{\log{n}} \quad ? \quad (\log{n})^n
    \\
    \\
    & \log{n^{(\log{n})}} \quad ? \quad \log{(\log{n})^n}
    \\ \implies
    & \log{n}.\log{n} \ll n.\log\log{n}
    \\
    \\ \implies
    & n^{\log{n}} \ll (\log{n})^n
\end{aligned}
$$

---

-   Compare growth of $2^n$ and $n^2$ and $\lfloor\log{n}\rfloor!$ and $\lfloor\log\log{n}\rfloor!$

We cannot compare these functions, so we compare **Logarithm** of these functions

$$
\begin{aligned}
    & 2^n \implies \log{2^n} = n \log{2}
    \\
    & n^2 \implies \log{n^2} = 2 \log{n}
    \\
    & \lfloor\log{n}\rfloor! \implies \log{\lfloor\log{n}\rfloor!} \simeq \log{n}.\log\log{n}
    \\
    & \lfloor\log\log{n}\rfloor! \implies \log{\lfloor\log\log{n}\rfloor!} \simeq \log\log{n}.\log\log\log{n}
    \\
    \\ \implies
    & n \log{2} \gg \log{n}.\log\log{n} \gg 2 \log{n} \gg \log\log{n}.\log\log\log{n}
    \\
    \\ \implies
    & 2^n \gg \lfloor\log{n}\rfloor! \gg n^2 \gg \lfloor\log\log{n}\rfloor!
\end{aligned}
$$

---

## Comparable GOF

Comparing GOF of functions can have 4 cases:

$$
f(n) \;?\; g(n) \implies
\begin{cases}
    f(n) \ll g(n): 3n^2 + 2n \ll n^3
    \\
    f(n) \gg g(n): 3n^2 + 2n \gg 2n
    \\
    f(n) \sim g(n): 3n^2 + 2n \sim n^2
    \\
    f(n) \nsim g(n): n \nsim n^{1+\sin{n}}
\end{cases}
$$

> -   $f(n) \sim g(n)$
> -   $f(n) \ll g(n)$
> -   $f(n) \gg g(n)$
> -   $f(n) \nsim g(n)$
>
> ![Order Of Growth](assets/order-of-growth.png)
>
> ![Unknown](assets/unknown.png)

> So we `cannot` compare growth of every two functions

---
