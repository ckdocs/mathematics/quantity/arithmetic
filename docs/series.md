# Series (Sigma)

> For finding sigma **similar formula** we can use **integral**

## Rules

### Factor Rule

$$
\begin{aligned}
    & \sum_{i=m}^{n} a.f(i) = a \sum_{i=m}^{n} f(i)
\end{aligned}
$$

### Split Rule

$$
\begin{aligned}
    & \sum_{i=m}^{n} ( f(i) + g(i)) = \sum_{i=m}^{n} f(i) + \sum_{i=m}^{n} g(i)
\end{aligned}
$$

### Telescopic Rule

$$
\begin{aligned}
    & \sum_{i=1}^{n} ( f(i) - f(i+1)) = f(1) - f(n+1)
\end{aligned}
$$

---

## Constant Serie

$$
\begin{aligned}
    & \sum_{i=m}^{n} a = a + a + \dots + a = (n-m+1).a
    \\
    \\
    & \int_{1}^{n} x dx = \frac{n^2}{2}
    \\
    \\
    Proof:
    \\
    & S = 1 + 2 + \dots + n
    \\
    & S = n + (n-1) + \dots + 1
    \\
    \\
    & 2S = (n+1) + (n+1) + \dots + (n+1)
    \\
    & 2S = n.(n+1)
    \\
    & S = \frac{n.(n+1)}{2}
\end{aligned}
$$

---

## Sum of powers Serie

$$
\begin{aligned}
    & \sum_{i=1}^{n} i = 1 + 2 + \dots + n = \frac{n.(n+1)}{2} \sim \frac{n^2}{2}
    \\
    \\
    & \int_{1}^{n} x dx = \frac{n^2}{2}
    \\
    \\
    Proof:
    \\
    & S = 1 + 2 + \dots + n
    \\
    & S = n + (n-1) + \dots + 1
    \\
    \\
    & 2S = (n+1) + (n+1) + \dots + (n+1)
    \\
    & 2S = n.(n+1)
    \\
    & S = \frac{n.(n+1)}{2}
\end{aligned}
$$

---

$$
\begin{aligned}
    & \sum_{i=1}^{n} i^2 = 1^2 + 2^2 + \dots + n^2 = \frac{n.(n+1).(2n+1)}{6} \sim \frac{n^3}{3}
    \\
    \\
    & \int_{1}^{n} x^2 dx = \frac{n^3}{3}
    \\
    \\
\end{aligned}
$$

---

$$
\begin{aligned}
    & \sum_{i=1}^{n} i^3 = 1^3 + 2^3 + \dots + n^3 = (1+2+\dots+n)^2 = \frac{n^2.(n+1)^2}{4} \sim \frac{n^4}{4}
    \\
    \\
    & \int_{1}^{n} x^3 dx = \frac{n^4}{4}
    \\
    \\
\end{aligned}
$$

---

$$
\begin{aligned}
    & \sum_{i=1}^{n} i^4 = 1^4 + 2^4 + \dots + n^4 \sim \frac{n^5}{5}
    \\
    \\
    & \int_{1}^{n} x^4 dx = \frac{n^5}{5}
    \\
    \\
\end{aligned}
$$

---

$$
\begin{aligned}
    & \sum_{i=1}^{n} i^p = 1^p + 2^p + \dots + n^p \sim \frac{n^{p+1}}{p+1}
    \\
    \\
    & \int_{1}^{n} x^p dx = \frac{n^{p+1}}{p+1}
    \\
    \\
\end{aligned}
$$

---

### Chess Plate Squares Count

If i was the plate size, then the total number of squares we can fit in chess plate with any size is:

$$
\begin{aligned}
    & Plate: 4 \times 4
    \\
    \\
    & 1 \times 1 = 4^4
    \\
    & 2 \times 2 = 3^4
    \\
    & 3 \times 3 = 2^4
    \\
    & 4 \times 4 = 1^4
    \\
    \\
    & Total: \sum_{i=1}^{4} i^2 = 1^2 + 2^2 + 3^2 + 4^2
\end{aligned}
$$

---

## Harmonic Serie

$$
\begin{aligned}
    & \sum_{i=1}^{n} \frac{1}{i} = \frac{1}{1} + \frac{1}{2} + \dots + \frac{1}{n} \sim Ln(n)
    \\
    \\
    & \int_{1}^{n} \frac{1}{x} dx = Ln(n) - Ln(1) = Ln(n) - 0 = Ln(n)
    \\
    \\
\end{aligned}
$$

---

$$
\begin{aligned}
    & \sum_{i=1}^{n} \frac{1}{i^2} = \frac{1}{1^2} + \frac{1}{2^2} + \dots + \frac{1}{n^2} \sim 1
    \\
    \\
    & \int_{1}^{n} \frac{1}{x^2} dx = (-\frac{1}{n}) - (-\frac{1}{1}) = 1 - \frac{1}{n}
    \\
    \\
\end{aligned}
$$

---
